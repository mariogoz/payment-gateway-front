import { NgModule , LOCALE_ID} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule }    from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { MeansComponent } from './means/means.component';

import { EfectivoComponent } from './means/sign/efectivo/efectivo.component';
import { SignEfectivoComponent } from './means/sign/efectivo/sign-efectivo/sign-efectivo.component';
import { CodeEfectivo } from './means/sign/efectivo/code-efectivo/code-efectivo.component';

import { TransferenciaComponent } from './means/sign/transferencia/transferencia.component';
import { DetailTransferenciaComponent } from './means/sign/transferencia/detail-transferencia/detail-transferencia.component';
import { NewTransferenciaComponent } from './means/sign/transferencia/new-user-trasnferencia/new-user-trasnferencia.component';
import { InitTransferenciaComponent } from './means/sign/transferencia/init-transferencia/init-transferencia.component';
import { FrequentTransferenciaComponent } from './means/sign/transferencia/frequent-user-transferencia/frequent-user-transferencia.component';

import { ErrorComponent } from './error/404/404.component';
import { ConfirmComponent } from './confirm/confirm.component';


const appRoutes: Routes = [
  { path: 'index/:id', component: MeansComponent,
    children: [
        {path: 'confirm' , component: ConfirmComponent}

      ] },
  { path: '404', component: ErrorComponent},
  { path: '**', redirectTo: '/404'}
];

@NgModule({
  declarations: [
    AppComponent,
    SidebarComponent,
    MeansComponent,
    EfectivoComponent,
    TransferenciaComponent,
    ErrorComponent,
    DetailTransferenciaComponent,
    NewTransferenciaComponent,
    InitTransferenciaComponent,
    FrequentTransferenciaComponent,
    SignEfectivoComponent,
    CodeEfectivo,
    ConfirmComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(
      appRoutes
    )
  ],
    providers: [ { provide: LOCALE_ID, useValue: 'en-CL' } ],
  bootstrap: [AppComponent]
})


export class AppModule {}
