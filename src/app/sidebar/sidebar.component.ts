import { Component, Input, OnChanges } from '@angular/core';
import { Router} from "@angular/router";
import { SidebarService } from './sidebar.services';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css'],
  providers: [SidebarService]
})
export class SidebarComponent implements OnChanges{
  @Input() gateway: any;
  pedido: any;
  surverList: any;
  reasonCancel: any;
  constructor(private sidebarService: SidebarService,
              private router: Router
            ) {

  }

  ngOnChanges() {
    this.pedido = this.gateway;
  }

  getSurvey(): void {
    this.sidebarService.getSurvey(this.gateway.gateway_id).subscribe(res => {
        this.surverList = res;
        console.log(this.surverList);
    });
  }

  confirm(): any {
    console.log(this.reasonCancel);
    this.doRedirectConfirm();
  }

  doRedirectConfirm() : void  {
    window.location.href = this.gateway.pedidoResult.url_retorno+"?id="+this.gateway.pedidoResult.id_publico;
  }
}
