import { Component, Input } from '@angular/core';
import { ActivatedRoute, Params, Router} from "@angular/router";
import { AppService } from './means.services';
@Component({
  selector: 'app-means-root',
  templateUrl: './means.component.html',
  styleUrls: ['./means.component.css'],
  providers: [ AppService ]
})
export class MeansComponent {
  viewEfectivo: boolean = false;
  viewTransferencia: boolean = false;
  publicId: number;
  represented: any;
  gateway: any;

constructor(private route: ActivatedRoute,
     private appService: AppService,
     private router: Router) {

     this.route.params.subscribe((params: Params) => {
       let paramId = params['id'];
       this.publicId = Number(paramId);
         if (this.publicId != null && this.publicId != undefined  && !isNaN(this.publicId) && this.publicId != 0) {
           this.getMethodPayment(this.publicId);
         } else {
           this.doRedirect404();
         }
     });
}

getMethodPayment(id: number): void  {

    this.appService.getPrueba(id).subscribe(res => {
        if (res.gateway_id == 1) {
          this.viewEfectivo = true;
          this.viewTransferencia  = false;
        } else if (res.gateway_id == 2) {
          this.viewEfectivo = false;
          this.viewTransferencia  = true;
        }
        this.gateway = res;
        this.represented = this.gateway.representadoResult;
    });
}

doRedirect404() : void  {
    this.router.navigate(['404']);
}

}
