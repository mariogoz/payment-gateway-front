import { Component, Input, AfterViewInit, OnInit, OnChanges } from '@angular/core';

@Component({
  selector: 'code-efectivo',
  templateUrl: './code-efectivo.component.html',
  styleUrls: ['./code-efectivo.component.css']
})
export class CodeEfectivo implements OnChanges {

@Input() pedido: any;
@Input() cupon: any;
fecha_expiracion: any;
code_format: any;


constructor() {
  this.doStorepoint();

}

ngOnChanges() {
  this.fecha_expiracion = new Date(this.pedido.pedidoResult.fecha_expiracion);
  this.code_format = this.formatCode();
}

formatCode(): string {
  let resultado = this.cupon.secuencia_cupon.match(/.{1,3}/g);
  return resultado[0] +"-"+resultado[1] +"-"+resultado[2];
}


onPrint(): void {
  window.print();
}

doStorepoint() {
  var a=document.createElement("script");
  a.type="text/javascript";
  a.async=!0;a.src="https://cdn.storepoint.co/assets/js/storepoint.min.js";
  var b=document.getElementsByTagName("script")[0];
  b.parentNode.insertBefore(a,b);
}

doRedirectConfirm() : void  {
  window.location.href = this.pedido.pedidoResult.url_retorno+"?id="+this.pedido.pedidoResult.id_publico;
}





}
