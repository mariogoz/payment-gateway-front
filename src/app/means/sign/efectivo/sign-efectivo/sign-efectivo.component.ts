import { Component, Output, EventEmitter, Input} from '@angular/core';
import { Validators, FormBuilder, FormGroup, FormControl} from '@angular/forms';
import { SingEfectivoServices } from './sign-efectivo.services';

@Component({
  selector: 'sign-efectivo',
  templateUrl: './sign-efectivo.component.html',
  styleUrls: ['./sign-efectivo.component.css'],
  providers: [SingEfectivoServices]
})
export class SignEfectivoComponent {

@Input() pedido: any;
@Output() initEfectivo = new EventEmitter<any>();

emailAnonimo: any;
//emailPattern ="([^.@]+)(\.[^.@]+)*@([^.@]+\.)+([^.@]+)";
emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";
emailIsValid: boolean = false;

  myform = this.formBuilder.group({
    email: ['', [Validators.required, Validators.pattern(this.emailPattern)]]

  });

  constructor(private formBuilder:FormBuilder, private singEfectivoServices: SingEfectivoServices){}

  generateCode(){
    if (this.myform.valid) {
      this.singEfectivoServices.generarCodigo(this.emailAnonimo,123123).subscribe(res => {
          this.initEfectivo.emit(res);
      });

    } else {
      this.emailIsValid = true;
    }
  }

  doStorepoint() {
    var a=document.createElement("script");
    a.type="text/javascript";
    a.async=!0;a.src="https://cdn.storepoint.co/assets/js/storepoint.min.js";
    var b=document.getElementsByTagName("script")[0];
    b.parentNode.insertBefore(a,b);
  }


}
