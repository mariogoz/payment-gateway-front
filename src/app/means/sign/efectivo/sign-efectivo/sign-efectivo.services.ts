import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import { map, catchError } from 'rxjs/operators';


@Injectable()
export class SingEfectivoServices {

  configUrl: string;
  constructor(public http:  HttpClient) { }

  generarCodigo(mail: string,id_publico: number): any {
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };
    let body = {user_email:mail,id_public:id_publico};
    this.configUrl = "https://localhost:8181/payment/cross/cash";
    console.log(this.configUrl);
    return this.http.post(this.configUrl,body, httpOptions)
    .pipe(
        map(res => res),
        catchError(this._serverError)
      );
  }

  private _serverError(err: any) {
        console.log('sever error:', err);  // debug
        return Observable.throw(err || 'Server Error');
    }
}
