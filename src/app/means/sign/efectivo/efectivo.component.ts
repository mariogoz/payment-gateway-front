import { Component, Input, AfterViewInit} from '@angular/core';
import { SignEfectivoComponent } from './sign-efectivo/sign-efectivo.component';
import { CodeEfectivo } from './code-efectivo/code-efectivo.component';

@Component({
  selector: 'efectivo',
  templateUrl: './efectivo.component.html',
  styleUrls: ['./efectivo.component.css'],
  providers: [SignEfectivoComponent, CodeEfectivo]
})
export class EfectivoComponent implements AfterViewInit{
  @Input() gateway: any;
  pedido: any;
  cupon: any;

  componentSign: boolean = true;
  componentCode: boolean = false;
  constructor() {

  }

  ngAfterViewInit () {
    this.pedido = this.gateway;
  }

  initEfectivo(val: any): void{
    this.cupon = val;
    console.log(this.cupon);
    this.componentSign= false;
    this.componentCode= true;
  }
}
