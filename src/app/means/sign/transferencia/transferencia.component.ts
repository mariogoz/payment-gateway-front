import { Component, ViewChild, Input} from '@angular/core';

import { DetailTransferenciaComponent } from './detail-transferencia/detail-transferencia.component';
import { NewTransferenciaComponent } from './new-user-trasnferencia/new-user-trasnferencia.component';
import { InitTransferenciaComponent } from './init-transferencia/init-transferencia.component';
import { FrequentTransferenciaComponent } from './frequent-user-transferencia/frequent-user-transferencia.component';

@Component({
  selector: 'transferencia',
  templateUrl: './transferencia.component.html',
  styleUrls: ['./transferencia.component.css'],
  providers: [ DetailTransferenciaComponent,
  NewTransferenciaComponent,
  InitTransferenciaComponent,
  FrequentTransferenciaComponent ]
})
export class TransferenciaComponent {
  @Input() gateway: any;

  agreed = 0;
  componentInit: boolean = true;
  componentNewUser: boolean = false;
  componentFrequent: boolean = false;
  componentDetail: boolean = false;
  user: any;

  constructor() {

  }

  onUser(val: any) {
    console.log(val);
    if (val.is_frencuente == true) {
      this.componentInit = false;
      this.componentFrequent = true;
    } else {
      this.componentInit = false;
      this.componentNewUser = true;
    }
    this.user = val;
  }

  confirmNewAcount (val: any) {
    this.componentNewUser = false;
    this.componentDetail = true;
  }

  confirmAcount (val: any) {
    console.log(val);
    this.componentFrequent = false;
    this.componentDetail = true;
  }

}
