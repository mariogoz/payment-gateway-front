export class Client {

  constructor(
    public name: string,
    public mail: string,
    public bank_id: string,
    public rut: string
  ) {  }

}
