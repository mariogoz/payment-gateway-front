import { Component, Input, OnInit, EventEmitter, Output } from '@angular/core';
import { TransferenciaServices } from '../transferencia.services';
import { Client } from '../Client';
@Component({
  selector: 'frequent-transferencia',
  templateUrl: './frequent-user-transferencia.component.html',
  styleUrls: ['./frequent-user-transferencia.component.css'],
  providers: [TransferenciaServices]
})
export class FrequentTransferenciaComponent implements OnInit{
  bankList: any;
  selectBank: any;
  client;
  @Output() confirmAcount = new EventEmitter<any>();
  @Input() user;

  constructor(private transferenciaServices: TransferenciaServices){
    this.transferenciaServices.getBank().subscribe(res => {
        this.bankList = res;
    });
  }
  ngOnInit () {

  }
  enviarFrequent() : void  {
    this.client = new Client(this.user.nombre,this.user.email,this.selectBank,this.user.rut);
    this.confirmAcount.emit(this.client);
  }
}
