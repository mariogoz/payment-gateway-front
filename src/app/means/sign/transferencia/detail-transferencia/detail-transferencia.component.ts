import { Component, Input } from '@angular/core';

@Component({
  selector: 'detail-transferencia',
  templateUrl: './detail-transferencia.component.html',
  styleUrls: ['./detail-transferencia.component.css']
})
export class DetailTransferenciaComponent {
  @Input() client;
}
