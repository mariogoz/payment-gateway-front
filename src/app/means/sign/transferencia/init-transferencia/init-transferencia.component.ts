import { Component, Input, Output, EventEmitter} from '@angular/core';
import { InitServices } from './init-transferencia.services';
import { Validators, FormBuilder, FormGroup, FormControl} from '@angular/forms';
@Component({
  selector: 'init-transferencia',
  templateUrl: './init-transferencia.component.html',
  styleUrls: ['./init-transferencia.component.css'],
  providers: [InitServices]
})
export class InitTransferenciaComponent {
  rut: string ;
  @Output() onUser = new EventEmitter<any>();

  rutPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";
  rutIsValid: boolean = false;

    myform = this.formBuilder.group({
      rut: ['', [Validators.required, Validators.pattern(this.rutPattern)]]

    });
  constructor(private initServices: InitServices, private formBuilder:FormBuilder){}

  confirmar(): void {
    // if (this.myform.valid) {
      this.initServices.getUser(this.rut).subscribe(res => {
          console.log(res);
          this.onUser.emit(res);
      });
    // } else {
      // this.rutIsValid = true;
    // }
  }
}
