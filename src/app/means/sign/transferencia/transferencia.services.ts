import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import { map, catchError } from 'rxjs/operators';


@Injectable()
export class TransferenciaServices {

  configUrl: string;
  constructor(public http:  HttpClient) { }

  getBank(): any {
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };
    this.configUrl = "https://localhost:8181/payment/cross/bank";
    console.log(this.configUrl);
    return this.http.get(this.configUrl, httpOptions)
    .pipe(
        map(res => res),
        catchError(this._serverError)
      );
  }

  private _serverError(err: any) {
        console.log('sever error:', err);  // debug
        return Observable.throw(err || 'Server Error');
    }
}
